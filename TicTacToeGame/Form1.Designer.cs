﻿namespace TicTacToeGame
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetScoreToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.BottomLeft = new System.Windows.Forms.Button();
            this.MiddleLeft = new System.Windows.Forms.Button();
            this.TopLeft = new System.Windows.Forms.Button();
            this.TopMiddle = new System.Windows.Forms.Button();
            this.MiddleMiddle = new System.Windows.Forms.Button();
            this.BottomMiddle = new System.Windows.Forms.Button();
            this.TopRight = new System.Windows.Forms.Button();
            this.MiddleRight = new System.Windows.Forms.Button();
            this.BottomRight = new System.Windows.Forms.Button();
            this.X_Score = new System.Windows.Forms.Label();
            this.O_Score = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.resetScoreToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(899, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.menuToolStripMenuItem.Text = "Reset Game ";
            // 
            // resetScoreToolStripMenuItem1
            // 
            this.resetScoreToolStripMenuItem1.Name = "resetScoreToolStripMenuItem1";
            this.resetScoreToolStripMenuItem1.Size = new System.Drawing.Size(79, 20);
            this.resetScoreToolStripMenuItem1.Text = "Reset Score";
            // 
            // BottomLeft
            // 
            this.BottomLeft.BackColor = System.Drawing.SystemColors.Desktop;
            this.BottomLeft.FlatAppearance.BorderSize = 0;
            this.BottomLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BottomLeft.Location = new System.Drawing.Point(117, 463);
            this.BottomLeft.Name = "BottomLeft";
            this.BottomLeft.Size = new System.Drawing.Size(219, 177);
            this.BottomLeft.TabIndex = 2;
            this.BottomLeft.UseVisualStyleBackColor = false;
            // 
            // MiddleLeft
            // 
            this.MiddleLeft.BackColor = System.Drawing.SystemColors.Desktop;
            this.MiddleLeft.FlatAppearance.BorderSize = 0;
            this.MiddleLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MiddleLeft.Location = new System.Drawing.Point(117, 280);
            this.MiddleLeft.Name = "MiddleLeft";
            this.MiddleLeft.Size = new System.Drawing.Size(219, 177);
            this.MiddleLeft.TabIndex = 3;
            this.MiddleLeft.UseVisualStyleBackColor = false;
            // 
            // TopLeft
            // 
            this.TopLeft.BackColor = System.Drawing.SystemColors.Desktop;
            this.TopLeft.FlatAppearance.BorderSize = 0;
            this.TopLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TopLeft.Location = new System.Drawing.Point(117, 97);
            this.TopLeft.Name = "TopLeft";
            this.TopLeft.Size = new System.Drawing.Size(219, 177);
            this.TopLeft.TabIndex = 4;
            this.TopLeft.UseVisualStyleBackColor = false;
            // 
            // TopMiddle
            // 
            this.TopMiddle.BackColor = System.Drawing.SystemColors.Desktop;
            this.TopMiddle.FlatAppearance.BorderSize = 0;
            this.TopMiddle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TopMiddle.Location = new System.Drawing.Point(342, 97);
            this.TopMiddle.Name = "TopMiddle";
            this.TopMiddle.Size = new System.Drawing.Size(219, 177);
            this.TopMiddle.TabIndex = 7;
            this.TopMiddle.UseVisualStyleBackColor = false;
            // 
            // MiddleMiddle
            // 
            this.MiddleMiddle.BackColor = System.Drawing.SystemColors.Desktop;
            this.MiddleMiddle.FlatAppearance.BorderSize = 0;
            this.MiddleMiddle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MiddleMiddle.Location = new System.Drawing.Point(342, 280);
            this.MiddleMiddle.Name = "MiddleMiddle";
            this.MiddleMiddle.Size = new System.Drawing.Size(219, 177);
            this.MiddleMiddle.TabIndex = 6;
            this.MiddleMiddle.UseVisualStyleBackColor = false;
            // 
            // BottomMiddle
            // 
            this.BottomMiddle.BackColor = System.Drawing.SystemColors.Desktop;
            this.BottomMiddle.FlatAppearance.BorderSize = 0;
            this.BottomMiddle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BottomMiddle.Location = new System.Drawing.Point(342, 463);
            this.BottomMiddle.Name = "BottomMiddle";
            this.BottomMiddle.Size = new System.Drawing.Size(219, 177);
            this.BottomMiddle.TabIndex = 5;
            this.BottomMiddle.UseVisualStyleBackColor = false;
            // 
            // TopRight
            // 
            this.TopRight.BackColor = System.Drawing.SystemColors.Desktop;
            this.TopRight.FlatAppearance.BorderSize = 0;
            this.TopRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TopRight.Location = new System.Drawing.Point(567, 97);
            this.TopRight.Name = "TopRight";
            this.TopRight.Size = new System.Drawing.Size(219, 177);
            this.TopRight.TabIndex = 10;
            this.TopRight.UseVisualStyleBackColor = false;
            // 
            // MiddleRight
            // 
            this.MiddleRight.BackColor = System.Drawing.SystemColors.Desktop;
            this.MiddleRight.FlatAppearance.BorderSize = 0;
            this.MiddleRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MiddleRight.Location = new System.Drawing.Point(567, 280);
            this.MiddleRight.Name = "MiddleRight";
            this.MiddleRight.Size = new System.Drawing.Size(219, 177);
            this.MiddleRight.TabIndex = 9;
            this.MiddleRight.UseVisualStyleBackColor = false;
            // 
            // BottomRight
            // 
            this.BottomRight.BackColor = System.Drawing.SystemColors.Desktop;
            this.BottomRight.FlatAppearance.BorderSize = 0;
            this.BottomRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BottomRight.Location = new System.Drawing.Point(567, 463);
            this.BottomRight.Name = "BottomRight";
            this.BottomRight.Size = new System.Drawing.Size(219, 177);
            this.BottomRight.TabIndex = 8;
            this.BottomRight.UseVisualStyleBackColor = false;
            // 
            // X_Score
            // 
            this.X_Score.AutoSize = true;
            this.X_Score.Location = new System.Drawing.Point(23, 38);
            this.X_Score.Name = "X_Score";
            this.X_Score.Size = new System.Drawing.Size(52, 13);
            this.X_Score.TabIndex = 11;
            this.X_Score.Text = "X Victory:";
            // 
            // O_Score
            // 
            this.O_Score.AutoSize = true;
            this.O_Score.Location = new System.Drawing.Point(23, 68);
            this.O_Score.Name = "O_Score";
            this.O_Score.Size = new System.Drawing.Size(53, 13);
            this.O_Score.TabIndex = 12;
            this.O_Score.Text = "O Victory:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.95683F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.94245F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.24461F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(105, 87);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.42857F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.57143F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 193F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(695, 570);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlText;
            this.ClientSize = new System.Drawing.Size(899, 662);
            this.Controls.Add(this.O_Score);
            this.Controls.Add(this.X_Score);
            this.Controls.Add(this.TopRight);
            this.Controls.Add(this.MiddleRight);
            this.Controls.Add(this.BottomRight);
            this.Controls.Add(this.TopMiddle);
            this.Controls.Add(this.MiddleMiddle);
            this.Controls.Add(this.BottomMiddle);
            this.Controls.Add(this.TopLeft);
            this.Controls.Add(this.MiddleLeft);
            this.Controls.Add(this.BottomLeft);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.ForeColor = System.Drawing.Color.DarkKhaki;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetScoreToolStripMenuItem1;
        private System.Windows.Forms.Button BottomLeft;
        private System.Windows.Forms.Button MiddleLeft;
        private System.Windows.Forms.Button TopLeft;
        private System.Windows.Forms.Button TopMiddle;
        private System.Windows.Forms.Button MiddleMiddle;
        private System.Windows.Forms.Button BottomMiddle;
        private System.Windows.Forms.Button TopRight;
        private System.Windows.Forms.Button MiddleRight;
        private System.Windows.Forms.Button BottomRight;
        private System.Windows.Forms.Label X_Score;
        private System.Windows.Forms.Label O_Score;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

    }
}

